
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
} from 'react-native';

export default class App extends Component {

  render() {
    return (
      <View style={styles.container}>
      <Image style={styles.logo} source={require('./logo.png')}/>
      <View style={styles.secondContainer}>
      <View style={styles.InputLabel}>
      <Text>Username</Text>
      </View>
      
        <View style={styles.inputContainer}>
        
          <TextInput style={styles.inputs}
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              />
        </View>
        <View style={styles.InputLabel}>
        <Text>Password</Text>
        </View>
        <View style={styles.inputContainer}>
          <TextInput style={styles.inputs}
              secureTextEntry={true}
              underlineColorAndroid='transparent'
              />
        </View>

        <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} >
          <Text style={styles.loginText}>Login</Text>
        </TouchableHighlight>
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#DCDCDC',
  },
  secondContainer: {
    justifyContent: 'center',
    paddingTop:30,
    paddingLeft:30,
    paddingRight:30,
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  },
  logo: {
    width:100,
    height:100,
    marginTop:-100,
    marginBottom:10
  },
  inputContainer: {
      
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#EBEDEF',
      borderBottomWidth: 1,
      width:250,
      height:45,
      marginTop:-10,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  InputLabel: {
    marginTop:-20,
    width:250,
    height:45,
    flexDirection: 'row',
    alignItems:'center',
    color:'#EBEDEF'
},
  inputs:{
      height:45,
      marginLeft:16,
      borderBottomColor: '#FFFFFF',
      flex:1,
  },
  inputIcon:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height:40,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:100,
    borderRadius:5,
    marginLeft:150
  },
  loginButton: {
    backgroundColor: "#00b5ec",
  },
  loginText: {
    color: 'white',
  }
});